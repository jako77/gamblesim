#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>

#include "MyRandom.h"
#include "Game.h"

using namespace std;

using runs_t = uint_fast64_t;

static void PrintWinTable( vector<win_t> const& winTable, ofstream& file );
static void SaveAllFromGameResults( vector<GameResult> const& gameResults );
static void RunAndSaveEveryXWinTable( player_t const players, price_t const price, runs_t const N, runs_t const X );
static void RunAndSaveWinTable( player_t const players, price_t const price, runs_t const N );
static void SaveMinimumFromGameResults( vector<GameResult> const& gameResults );
static vector<GameResult> RunAndGetGameResults( player_t players, price_t price, runs_t N );
static void RunAndTime( int mode );

static player_t players = 10;
static runs_t N = 10'000'000;
static price_t price = 10'000;

int main( const int argc, const char** argv )
{
    MyRandom::SetSeedFromCurrentTime();

    int mode = 0;    
    if ( argc == 5 )
    {
        players = atoi( argv[1] );
        price = atoi( argv[2] );
        N = atoll( argv[3] );
        mode = atoi( argv[4] );
    }
    else
    {
        cout << "Usage: <players> <price> <N> <mode>\n";
        return -1;
    }   

    cout << "Starting seed: " << MyRandom::Seed << "\n";

    RunAndTime( mode );

    return 0;
}

static void RunAndTime( int mode )
{
    auto start = std::chrono::high_resolution_clock::now();

    if ( mode == 0 )
    {
        // Play N games and save minimum amount to file
        auto gameResults = RunAndGetGameResults( players, price, N );
        SaveMinimumFromGameResults( gameResults );
    }
    else if ( mode == 1 )
    {
        // Play N games and save every result to file
        auto gameResults = RunAndGetGameResults( players, price, N );
        SaveAllFromGameResults( gameResults );
    }
    else if ( mode == 2 )
    {
        // Play N games and save the win table each iteration
        RunAndSaveWinTable( players, price, N );
    }
    else if ( mode == 3 )
    {
        // Play N games and save every 1000 iteration of the win table
        RunAndSaveEveryXWinTable( players, price, N, 1000 );
    }

    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";
}

static vector<GameResult> RunAndGetGameResults( player_t players, price_t price, runs_t N )
{
    vector<GameResult> gameResults {};
    gameResults.reserve( N );
    
    for ( runs_t i = 0; i < N; i++ )
    {
        gameResults.push_back( Game::Play( players, price ) );
    }

    return gameResults;
}

static void SaveMinimumFromGameResults( vector<GameResult> const& gameResults )
{
    ofstream file;
    file.open("output.csv");

    file << "winner,loser,amount\n";

    for ( runs_t i = 0; i < N; i++ )
    {
        file << gameResults[ i ].WinnerIndex << ","
            << gameResults[ i ].LoserIndex << ","
            << gameResults[ i ].ExchangeAmount << "\n";
    }

    file.close();
}

static void RunAndSaveWinTable( player_t const players, price_t const price, runs_t const N )
{
    RunAndSaveEveryXWinTable( players, price, N, 1 );
}

static void RunAndSaveEveryXWinTable( player_t const players, price_t const price, runs_t const N, runs_t const X )
{
    vector<win_t> winTable( players, 0 );

    ofstream file;
    file.open("output.csv");

    // header
    file << "Index" << ",";

    file << "Player0";
    for ( player_t i = 1; i < players; i++ )
    {
        file << "," << "Player" << i;
    }

    file << "\n";

    // body
    for ( runs_t i = 0; i < N; i++ )
    {
        Game::Play( players, price, winTable );

        if ( i % X == 0 )
        {
            file << i << ",";
            PrintWinTable( winTable, file );
        }
    }

    file.close();
}

static void SaveAllFromGameResults( vector<GameResult> const& gameResults )
{
    runs_t N = gameResults.size();
    player_t players = gameResults[0].Rolls.size();

    ofstream file;
    file.open("output.csv");

    file << "Index,WinnerIndex,LoserIndex,Exchange,Rolls0;Rolls1;...RollsP\n";

    for ( runs_t i = 0; i < N; i++ )
    {
        file << i << "," 
            << gameResults[i].WinnerIndex << "," 
            << gameResults[i].LoserIndex << ","
            << gameResults[i].ExchangeAmount << ",";

        file << gameResults[i].Rolls[0];
        for ( player_t p = 1; p < players; p++ )
        {
            file << ";" << gameResults[i].Rolls[p];
        }

        file << "\n";
    }

    file.close();
}

static void PrintWinTable( vector<win_t> const& winTable, ofstream& file )
{
    file << winTable[ 0 ];
    for ( player_t j = 1; j < players; j++ )
    {
        file << "," << winTable[ j ];
    }

    file << "\n";
}