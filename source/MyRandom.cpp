#include <stdlib.h>
#include "MyRandom.h"

#include <chrono>
#include <ctime>

const RNG_t multiplier = ( RNG_t ) 2862933555777941757;
const RNG_t addend = ( RNG_t ) 3037000493;
const RNG_t RNG_MAX = ( RNG_t ) 18446744073709551615ull;

static RNG_t MyRand();

RNG_t MyRandom::Seed = 1234;

double MyRandom::NextDoubleNorm()
{
    return double( MyRand() ) / RNG_MAX;
}

int MyRandom::NextInt( int const minValue, int const maxValue )
{
    int range = maxValue - minValue;
    float floatValue = MyRandom::NextDoubleNorm();
    return ( range * floatValue ) + minValue;
}

void MyRandom::SetSeed( unsigned const seed )
{
    Seed = seed;
}

void MyRandom::SetSeedFromCurrentTime()
{
    auto currentTime = std::chrono::system_clock::now();
    auto duration = currentTime.time_since_epoch();
    auto millis = std::chrono::duration_cast<std::chrono::milliseconds>( duration ).count();

    MyRandom::SetSeed( millis );
    MyRandom::SetSeed( MyRand() );
}

static RNG_t MyRand()
{
    MyRandom::Seed = ( multiplier * MyRandom::Seed + addend ) % RNG_MAX;

    return MyRandom::Seed;
}
