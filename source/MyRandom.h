#pragma once

#include <stdint.h>

using RNG_t = uint64_t;

class MyRandom
{
public:

    static RNG_t Seed;

    static double NextDoubleNorm();
    static void SetSeed( unsigned const seed );
    static void SetSeedFromCurrentTime();
    static int NextInt( int const minValue, int const maxValue );

private:
    MyRandom() {}
};