#include <iostream>
#include <assert.h>

#include "Game.h"
#include "MyRandom.h"

#define RunTest( func )                                 \
do                                                      \
{                                                       \
    cout << "Test: " << #func << "......";              \
    func();                                             \
    cout << "Done\n";                                   \
} while ( 0 );

void ValidateGameMinMax();
void SameSeedYieldsSameRolls();

int main( const int argc, const char** argv )
{
    cout << "__________Running all tests__________\n";

    RunTest( ValidateGameMinMax );
    RunTest( SameSeedYieldsSameRolls );

    return 0;
}

void ValidateGameMinMax()
{
    vector<win_t> vect1 { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    player_t minIndex = 20;
    player_t maxIndex = 20;

    Game::GetMinMax( vect1, 10, maxIndex, minIndex );

    assert( minIndex == 0 );
    assert( maxIndex == 9 );

    vector<win_t> vect2 { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

    Game::GetMinMax( vect2, 10, maxIndex, minIndex );

    assert( minIndex == 9 );
    assert( maxIndex == 0 );

    vector<win_t> vect3 { 8, 7, 6, 5, 0, 9, 4, 3, 2, 1 };

    Game::GetMinMax( vect3, 10, maxIndex, minIndex );

    assert( minIndex == 4 );
    assert( maxIndex == 5 );
}

void SameSeedYieldsSameRolls()
{
    MyRandom::SetSeed( 1234 );
    vector<price_t> rolls_1( 10, 0 );
    vector<price_t> rolls_2( 10, 0 );

    Game::Roll( 10, 10000, rolls_1 );
    Game::Roll( 10, 10000, rolls_2 );
    
    MyRandom::SetSeed( 1234 );  
    vector<price_t> rolls_3 = Game::Roll( 10, 10000 );
    vector<price_t> rolls_4 = Game::Roll( 10, 10000 );

    for ( player_t i = 0; i < 10; i++ )
    {
        assert( rolls_1[ i ] == rolls_3[ i ] );
        assert( rolls_2[ i ] == rolls_4[ i ] );
        assert( rolls_1[ i ] != rolls_2[ i ] );
    }
}