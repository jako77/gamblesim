#pragma once

#include <vector>
#include <stdint.h>

using player_t  = uint_fast16_t;    // data size for storing number of players
using price_t = uint_fast16_t;      // data size for storing price/rolls
using win_t = uint_fast64_t;        // data size for storing aggregated wins

using namespace std;

struct GameResult
{
    vector<price_t> Rolls;
    player_t WinnerIndex;
    player_t LoserIndex;
    price_t ExchangeAmount;
};

namespace Game
{
    GameResult Play( player_t const players, price_t const price );
    void Play( player_t const players, price_t const price, GameResult& gameResult );
    void Play( player_t const players, price_t const price, vector<win_t>& winTable );
    vector<price_t> Roll( player_t const players, price_t const price );
    void Roll( player_t const players, price_t const price, vector<price_t>& rollVector );
    void GetMinMax( vector<win_t> const& vect, player_t const vectorSize, player_t& maxIndex, player_t& minIndex );
}