#include "Game.h"
#include "MyRandom.h"

// Play and return all results
GameResult Game::Play( player_t const players, price_t const price )
{
    struct GameResult gameResult;
    
    gameResult.Rolls = Game::Roll( players, price );

    GetMinMax( gameResult.Rolls, players, gameResult.WinnerIndex, gameResult.LoserIndex );

    gameResult.ExchangeAmount = gameResult.Rolls[ gameResult.WinnerIndex ] 
                                - gameResult.Rolls[ gameResult.LoserIndex ];

    return gameResult;
}

// Play and return all results by ref
void Game::Play( player_t const players, price_t const price, GameResult& gameResult )
{
    gameResult.Rolls = Game::Roll( players, price );

    GetMinMax( gameResult.Rolls, players, gameResult.WinnerIndex, gameResult.LoserIndex );

    gameResult.ExchangeAmount = gameResult.Rolls[ gameResult.WinnerIndex ] 
                                - gameResult.Rolls[ gameResult.LoserIndex ];
}

// Play and update the wins table (assumes it is filled with zeros)
void Game::Play( player_t const players, price_t const price, vector<win_t>& winTable )
{
    auto rolls = Game::Roll( players, price );

    player_t maxIndex;
    player_t minIndex;
    GetMinMax( rolls, players, maxIndex, minIndex );

    auto exchangeAmount = rolls[ maxIndex ] - rolls[ minIndex ];

    winTable[ maxIndex ] += exchangeAmount;
    winTable[ minIndex ] -= exchangeAmount;
}

// Roll and return a new vector
vector<price_t> Game::Roll( player_t const players, price_t const price )
{
    vector<price_t> rollVector {};
    rollVector.reserve( players );

    for ( player_t i = 0; i < players; i++ )
    {
        rollVector.push_back( MyRandom::NextInt( 1, price ) );
    }

    return rollVector;
}

// Put rolls in pre-allocated vector (e.g. with zeros)
void Game::Roll( player_t const players, price_t const price, vector<price_t>& rollVector )
{
    for ( player_t i = 0; i < players; i++ )
    {
        rollVector[ i ] = MyRandom::NextInt( 1, price );
    }
}

void Game::GetMinMax( vector<win_t> const& vect, player_t const vectorSize, player_t& maxIndex, player_t& minIndex )
{
    maxIndex = 0;
    minIndex = 0;
    for ( player_t i = vectorSize - 1; i > 0; i-- )
    {
        if ( vect[ i ] > vect[ maxIndex ] )
            maxIndex = i;
        else if ( vect[ i ] < vect [ minIndex ] )
            minIndex = i;
    }
}


