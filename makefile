ifdef DEBUG
	DEBUG_CFLAGS = -g
endif

CC = g++ -Ofast
CFLAGS = -Wall $(DEBUG_CFLAGS)
SOURCE_PATH = source
BUILD_PATH = build
BIN_PATH = binary

main: Main.o Test
	$(CC) $(CFLAGS) -o $(BIN_PATH)/run $(BUILD_PATH)/Main.o $(BUILD_PATH)/MyRandom.o $(BUILD_PATH)/Game.o

Main.o: MyRandom.o MyRandom.o Game.o
	$(CC) $(CFLAGS) -c $(SOURCE_PATH)/Main.cpp -o $(BUILD_PATH)/Main.o

MyRandom.o:
	$(CC) $(CFLAGS) -c $(SOURCE_PATH)/MyRandom.cpp -o $(BUILD_PATH)/MyRandom.o

Game.o: MyRandom.o
	$(CC) $(CFLAGS) -c $(SOURCE_PATH)/Game.cpp -o $(BUILD_PATH)/Game.o

Test.o: Game.o
	$(CC) $(CFLAGS) -c $(SOURCE_PATH)/Test.cpp -o $(BUILD_PATH)/Test.o

Test: MyRandom.o Game.o Test.o
	$(CC) $(CFLAGS) -o $(BIN_PATH)/test $(BUILD_PATH)/Test.o $(BUILD_PATH)/MyRandom.o $(BUILD_PATH)/Game.o

clean:
	$(RM) $(BUILD_PATH)/*
	$(RM) $(BIN_PATH)/*

fresh: clean main